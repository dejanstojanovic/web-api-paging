﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Sample.Web.Api.Controllers
{
    public class Values1Controller : Controller
    {

        public class FilterModel
        {
            public String Term { get; set; }
            public DateTime MinDate { get; set; }
            public Boolean IncludeInactive { get; set; }
            public int Page { get; set; }
            public int Limit { get; set; }
        }

        public class PagedCollectionResponse<T> where T : class
        {
            public IEnumerable<T> Items { get; set; }
            public Uri NextPage { get; set; }
            public Uri PreviousPage { get; set; }
        }

        public class Person
        {
            public String Name { get; set; }
            public DateTime DOB { get; set; }
            public String Email { get; set; }
        }

        [HttpGet]
        public ActionResult<PagedCollectionResponse<Person>> Get([FromQuery] FilterModel filter)
        {
            //Handle filtering and paging
        }

        //[HttpGet]
        //public IActionResult Get(String term, int page, int limit, DateTime? minDate = null, Boolean includeInactive=false)
        //{
        //    //Handle filtering and paging
        //}
    }
}